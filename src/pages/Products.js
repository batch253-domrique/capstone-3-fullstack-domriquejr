import { useEffect, useState } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import ProductCard from '../components/ProductCard';

export default function Products() {
    const [product, setProducts] = useState([]);

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products`)
            .then((res) => res.json())
            .then((data) => {
                console.log(data);

                setProducts(
                    data.map((product) => {
                        return <ProductCard key={product._id} product={product} />;
                    })
                );
            });
    }, []);

    return (
        <>
            <Container fluid>
                <Row>
                    <Col md={3}>{product}</Col>
                </Row>
            </Container>
        </>
    );
}
