import { useEffect, useState } from 'react';
import { Container, Row } from 'react-bootstrap';
import A_SuperMainCard from '../components/A_SuperMainCard';
import GetActive from '../components/GetActive';

export default function Products() {
    const [product, setProducts] = useState([]);

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/active`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`,
            },
        })
            .then((res) => res.json())
            .then((data) => {
                console.log(data);

                setProducts(
                    data.map((product) => {
                        return <A_SuperMainCard key={product._id} product={product} />;
                    })
                );
            });
    }, []);

    return (
        <>
            <GetActive />
            <Container>
                <Row>{product}</Row>
            </Container>
        </>
    );
}
