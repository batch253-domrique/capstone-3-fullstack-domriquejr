import { useState, useEffect, useContext } from 'react';
import { Form, Button, Row, Col, Container } from 'react-bootstrap';
import { Navigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login() {
    const { user, setUser } = useContext(UserContext);
    const [agreeToTerms, setAgreeToTerms] = useState(false);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(false);

    const handleCheckboxChange = (e) => {
        setAgreeToTerms(e.target.checked);
    };

    function authenticate(e) {
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email: email,
                password: password,
            }),
        })
            .then((res) => res.json())
            .then((data) => {
                if (typeof data.access !== 'undefined') {
                    localStorage.setItem('token', data.access);
                    retrieveUserDetails(data.access);
                    if (data.isAdmin) {
                        Swal.fire({
                            title: 'Login Successful',
                            icon: 'success',
                            text: 'Welcome back, Master!',
                        });
                    } else {
                        Swal.fire({
                            title: 'Login Successful',
                            icon: 'success',
                            text: 'Get ready to experience something truly amazing!',
                        });
                    }
                } else {
                    Swal.fire({
                        title: 'Authentication Failed',
                        icon: 'error',
                        text: 'Check your login credentials and try again.',
                    });
                }
            });

        setEmail('');
        setPassword('');
    }

    const retrieveUserDetails = (token) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/:userId/userDetails`, {
            headers: {
                Authorization: `Bearer ${token}`,
            },
        })
            .then((res) => res.json())
            .then((data) => {
                console.log(data);

                setUser({
                    id: data._id,
                    isAdmin: data.isAdmin,
                });
            });
    };

    useEffect(() => {
        if (email !== '' && password !== '') {
            setIsActive(true);
        } else {
            setIsActive(false);
        }
    }, [email, password]);

    return user.id !== null ? (
        <>{user.isAdmin === true ? <Navigate to="/adminDashboard" /> : <Navigate to="/cxMain" />}</>
    ) : (
        <>
            <Container fluid style={{ position: 'relative' }}>
                <video
                    autoPlay
                    loop
                    muted
                    style={{ width: '100%', height: '100%', objectFit: 'cover', opacity: 0.2 }}
                >
                    <source
                        src="https://res.cloudinary.com/dv3x7qoak/video/upload/v1679817639/login_kdzlfl.mp4"
                        type="video/mp4"
                    />
                </video>
                <Row
                    className="justify-content-center"
                    style={{ position: 'absolute', top: 0, left: 0, right: 0, bottom: 0 }}
                >
                    <Col md={4} lg={4}>
                        <Form
                            onSubmit={(e) => authenticate(e)}
                            style={{
                                border: '1px solid #ccc',
                                padding: '20px',
                                boxShadow: '0px 0px 10px rgba(0, 0, 0, 0.2)',
                            }}
                            className="rounded-4 mt-5 mb-5 px-5  pb-5 "
                        >
                            <Col md={12} lg={12}>
                                <h3 className="text-center mb-3">
                                    <img
                                        src="https://res.cloudinary.com/dv3x7qoak/image/upload/v1679811880/carrot_dvb09c.png"
                                        className="img-fluid my-md-3"
                                        style={{
                                            maxWidth: '70px',
                                            maxHeight: '70px',
                                        }}
                                    />
                                    Login
                                </h3>
                                <h4 className="mb-4">Revolutionize your online shopping </h4>

                                <Form.Group controlId="userEmail">
                                    {/* <Form.Label>Email address</Form.Label> */}
                                    <Form.Control
                                        type="email"
                                        placeholder="Enter email"
                                        value={email}
                                        onChange={(e) => setEmail(e.target.value)}
                                        required
                                        className="mb-4"
                                    />
                                </Form.Group>

                                <Form.Group controlId="password" className="mb-md-4">
                                    {/* <Form.Label>Password</Form.Label> */}
                                    <Form.Control
                                        type="password"
                                        placeholder="Password"
                                        value={password}
                                        onChange={(e) => setPassword(e.target.value)}
                                        required
                                    />
                                </Form.Group>
                                <Form.Group controlId="termsCheckbox">
                                    <Form.Check
                                        type="checkbox"
                                        label="I agree to the privacy policy and terms."
                                        checked={agreeToTerms}
                                        onChange={handleCheckboxChange}
                                        required
                                    />
                                </Form.Group>

                                <Link to={`/register`}>or create an account</Link>
                                <br />
                            </Col>
                            <Col md={12} lg={12}>
                                {isActive && agreeToTerms ? (
                                    <Button
                                        className="mt-4 mb-2 w-100"
                                        variant="success"
                                        type="submit"
                                        id="submitBtn"
                                    >
                                        Submit
                                    </Button>
                                ) : (
                                    <Button
                                        className="mt-4 mb-2 w-100 "
                                        variant="danger"
                                        type="submit"
                                        id="submitBtn"
                                        disabled
                                    >
                                        Submit
                                    </Button>
                                )}
                            </Col>
                        </Form>
                    </Col>
                </Row>
            </Container>
        </>
    );
}
