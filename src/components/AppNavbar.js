import { useContext } from 'react';
import { Container, Navbar, Nav, Form, Button } from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';
import icons from '../images/icons.png';
import out from '../images/logout.png';
// import logo from '../images/logo.png';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function AppNavbar() {
    const { user } = useContext(UserContext);
    const navigate = useNavigate();
    function handleLogout() {
        Swal.fire({
            title: 'Are you sure you want to logout?',
            icon: 'https://res.cloudinary.com/dv3x7qoak/image/upload/v1679811139/carrot_msxkvy.png',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, logout!',
        }).then((result) => {
            if (result.isConfirmed) {
                Swal.fire('Thank you!', 'Feel free to make another purchase anytime.', 'success');
                navigate('/logout');
            }
        });
    }

    return (
        <Navbar bg="light" expand="lg">
            <Container fluid className="logoss mx-md-4">
                {user.isAdmin === true ? (
                    <Navbar.Brand as={Link} to="/adminDashboard">
                        <img
                            src="https://www.instacart.com/assets/beetstrap/brand/2022/instacart-logo-color-6678cb82d531f8910d5ba270a11a7e9b56fc261371bda42ea7a5abeff3492e1c.svg"
                            style={{
                                maxWidth: '150px',
                                maxHeight: '150px',
                                padding: 0,
                                margin: 0,
                            }}
                        ></img>
                    </Navbar.Brand>
                ) : (
                    <Navbar.Brand>
                        <img
                            src="https://www.instacart.com/assets/beetstrap/brand/2022/instacart-logo-color-6678cb82d531f8910d5ba270a11a7e9b56fc261371bda42ea7a5abeff3492e1c.svg"
                            className="img-fluid"
                            style={{
                                maxWidth: '150px',
                                maxHeight: '150px',
                                padding: 0,
                                margin: 0,
                            }}
                        ></img>
                    </Navbar.Brand>
                )}
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="ms-auto">
                        {user.isAdmin === true && user.id !== null ? (
                            <>
                                <Nav.Link as={NavLink} to="/adminDashboard">
                                    Dashboard
                                </Nav.Link>

                                <Nav.Link as={NavLink} to="/catalog">
                                    Product Catalog
                                </Nav.Link>

                                <img
                                    onClick={handleLogout}
                                    src={out}
                                    className="img-fluid me-3"
                                    style={{
                                        maxWidth: '35px',
                                        maxHeight: '35px',
                                        padding: 0,
                                        marginTop: '5px',
                                        transition: 'transform 0.3s ease',
                                    }}
                                    onMouseOver={(e) =>
                                        (e.currentTarget.style.transform = 'scale(1.2)')
                                    }
                                    onMouseOut={(e) =>
                                        (e.currentTarget.style.transform = 'scale(1)')
                                    }
                                    title="Logout"
                                />
                            </>
                        ) : user.id !== null && user.isAdmin === false ? (
                            <>
                                {/* shop now Icon */}

                                <Nav.Link as={NavLink} to="/cxProduct">
                                    <img
                                        src={icons}
                                        className="img-fluid me-2"
                                        style={{
                                            maxWidth: '35px',
                                            maxHeight: '35px',
                                            padding: 0,
                                            margin: 0,
                                            transition: 'transform 0.3s ease',
                                        }}
                                        onMouseOver={(e) =>
                                            (e.currentTarget.style.transform = 'scale(1.2)')
                                        }
                                        onMouseOut={(e) =>
                                            (e.currentTarget.style.transform = 'scale(1)')
                                        }
                                        title="Store"
                                    />
                                </Nav.Link>
                                <img
                                    onClick={handleLogout}
                                    src={out}
                                    className="img-fluid me-3"
                                    style={{
                                        maxWidth: '35px',
                                        maxHeight: '35px',
                                        padding: 0,
                                        marginTop: '5px',
                                        transition: 'transform 0.3s ease',
                                    }}
                                    onMouseOver={(e) =>
                                        (e.currentTarget.style.transform = 'scale(1.2)')
                                    }
                                    onMouseOut={(e) =>
                                        (e.currentTarget.style.transform = 'scale(1)')
                                    }
                                    title="Logout"
                                />
                            </>
                        ) : (
                            <>
                                <Nav.Link as={NavLink} to="/login">
                                    Login
                                </Nav.Link>
                                <Nav.Link as={NavLink} to="/register">
                                    Sign Up
                                </Nav.Link>
                            </>
                        )}
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
}
