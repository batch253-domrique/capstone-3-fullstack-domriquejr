//NOTES: THIS IS ONLY FOR ADMIN PAGE
//THIS IS USE TO SHOW THE LIST OF ACTIVE PRODUCTS ONLY
//YOU CAN ALSO SEE THE DETAILS OF THE PRODUCT BUT YOU CAN'T EDIT

import { Card, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function ProductCard({ product }) {
    const { _id, name, description, price, image } = product;

    return (
        // </Card>
        <Col xs={12} md={3} className="mt-3 mb-3">
            <Card className="cardHighlight">
                <Card.Body>
                    <Card.Img src={image} className="img-fluid" />
                    <Card.Title>
                        <h5 className="product-names">{name}</h5>
                    </Card.Title>
                    <Card.Text>{description}</Card.Text>
                    <Card.Text className="price-color">
                        <p>
                            <span>&#8369;</span> {price}
                        </p>
                    </Card.Text>

                    <Link className="btn btn-primary mx-2" to={`/products/${_id}/catalogView`}>
                        Details
                    </Link>
                </Card.Body>
            </Card>
        </Col>
    );
}
