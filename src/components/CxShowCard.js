//NOTES: THIS IS FOR ADMIN'S EYE ONLY TO SHOW ALL THE MAPPED USERS THAT WAS ALREADY FETCHED
import UserContext from '../UserContext';
import { useState } from 'react';
import { Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function ProductCard() {
    const { user } = useState(UserContext);

    return (
        <Card className="my-3">
            <Card.Body>
                <Card.Title>{user.orderedProduct}</Card.Title>
                <Card.Title>{user}</Card.Title>
            </Card.Body>
        </Card>
    );
}
