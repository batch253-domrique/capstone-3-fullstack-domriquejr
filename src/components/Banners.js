import React from 'react';
import { Carousel } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

function CarouselComponent() {
    return (
        <Carousel
            interval={1500}
            slide={true}
            controls={false}
            style={{ marginBottom: '0', borderRadius: '10px' }}
        >
            <Carousel.Item>
                <img
                    className="d-block w-100"
                    src="https://images.prismic.io/shopsm/76843f76-2978-4108-a2df-fd7c6321409d_SUMMER+OUTDOOR-1800X600+v2+%281%29.png?auto=format&rect=0%2C0%2C1800%2C600&ixlib=react-9.5.2&w=1800&h=600&dpr=1&q=75"
                    alt="First Slide"
                />
            </Carousel.Item>
            <Carousel.Item>
                <img
                    className="d-block w-100"
                    src="https://images.prismic.io/shopsm/da735a84-466c-4f13-a251-f3f8d7972bd5_MAIN+BANNER+1800x600+%283%29.jpg?auto=format&rect=0%2C0%2C1800%2C600&ixlib=react-9.5.2&w=1800&h=600&dpr=1&q=75"
                    alt="Second Slide"
                />
            </Carousel.Item>
            <Carousel.Item>
                <img
                    className="d-block w-100"
                    src="https://images.prismic.io/shopsm/b9c51584-b756-41f4-bf44-919c61f7f247_2nd+Banner+%281%29.jpg?auto=format&rect=0%2C0%2C1800%2C600&ixlib=react-9.5.2&w=1800&h=600&dpr=1&q=75"
                    alt="Third Slide"
                />
            </Carousel.Item>
        </Carousel>
    );
}

export default CarouselComponent;
